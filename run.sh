#!/bin/sh -e

check_connection() {
  url="${1}"
  if nc -z -w 2 "${1}" 443; then
    echo ok
  fi
}

usage() {
  echo "Usage: $0 [options] [host1 host2 host3 ...]
Options are:
 --offline avoid updating repository
 --ext_debs fetches external debs from the directory ext_debs
 --help print usage"
  exit 0
}

main() {
  cd "$(dirname "${0}")"

  # specially useful if you get into infinite timeout caused by fail2ban
  # TODO use a ungleich mirror repo
  check_ungleich="$(check_connection "code.ungleich.ch")"

  if [ ! -d cdist ]; then
    if [ -n "${check_ungleich}" ]; then
      #git clone https://code.ungleich.ch/ungleich-public/cdist.git cdist
      git clone https://code.ungleich.ch/pedro/cdist.git -b bugfix_363
    fi
  fi

  # Parse input parameters
  while [ $# -gt 0 ]; do
    case "${1}" in
      # If it is not desired to auto-update repos, pass --offline as first arg
      "--offline")
        shift
        offline=n
        ;;
      # download all external debs
      "--ext_debs")
        ext_debs_opt="--ext_debs_dir ext_debs"
        shift

        ext_debs/wget.sh

        ;;
      "--help")
        usage
        ;;
      # host detected
      *)
        break
        ;;
    esac
  done

  # detect other options after host part started and report as an error
  if echo "${@}" | grep "\-\-" >/dev/null 2>&1; then
    usage
    exit 1
  fi

  if [ -z "${offline}" ]; then
    cd cdist; echo "\n\ngit pull cdist repo" > /dev/stderr
    if [ -n "${check_ungleich}" ]; then
      git pull
    else
      echo '  cannot update cdist repo because is unavailable, continue'
    fi
    cd - > /dev/null
  fi

  echo "__pedrolab_basic_xfce_desktop ${ext_debs_opt}" | cdist/bin/cdist config -c . -j -v -i - "${@}"
}

main "${@}"
