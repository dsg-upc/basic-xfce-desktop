#!/bin/sh -e

# inspired on https://gitlab.com/guifi-exo/wiki/-/blob/master/howto/VMs-procedures/bootstrap.sh

stderr() {
  printf "%s" "$@" >&2
  false
}

# aux functions
get_user_home() {
  getent passwd "${1}" | cut -d: -f6
}
prepare_ssh_dir() {
  local myuser="${1}"
  # Get home directory
  local mypath="$(get_user_home "${myuser}")"
  local authkeys_file="${mypath}/.ssh/authorized_keys"
  mkdir -p "${mypath}/.ssh/"
  if [ ! -f "${authkeys_file}" ]; then
    # Create authorized_keys file only if needed
    touch "${mypath}"/.ssh/authorized_keys
  fi
  chmod 0700 "${mypath}"/.ssh/
  chmod 0600 "${mypath}"/.ssh/authorized_keys
  local key_path="${mypath}/.ssh/id_rsa"
  if [ ! -f "${key_path}" ]; then
    ssh-keygen -b 4096 -t rsa -f "${key_path}" -q -N ""
  fi
  chown -R "${myuser}:" "${mypath}"/.ssh/
  find "${mypath}"/.ssh/ -type d -exec chmod 0700 {} \;
  find "${mypath}"/.ssh/ -type f -exec chmod 0600 {} \;

  if [ -f "${SSH_KEY}" ]; then
    cat "${SSH_KEY}" > "${authkeys_file}"
  fi
}

fetch_git_repo() {
  local url="$1"
  local dir="$2"
  if [ ! -d "${dir}/.git" ]; then
    echo "Fetching git repo: ${dir}" >&2
    git clone "${url}" "${dir}"
  else
    (
      cd "${mydir}"
      printf "\n\ngit pull $(basename "${dir}"):\n" >&2
      git pull
    )
  fi
}

# decide_if_update inspired from Ander in https://code.ungleich.ch/ungleich-public/cdist/-/issues/862
decide_if_update() {
  if [ ! -d /var/lib/apt/lists ] \
    || [ -n "$( find /etc/apt -newer /var/lib/apt/lists )" ] \
    || [ ! -f /var/cache/apt/pkgcache.bin ] \
    || [ "$( stat --format %Y /var/cache/apt/pkgcache.bin )" -lt "$( date +%s -d '-1 day' )" ]
  then
    if [ -d /var/lib/apt/lists ]; then
      touch /var/lib/apt/lists
    fi
    apt_opts="-o Acquire::AllowReleaseInfoChange::Suite=true -o Acquire::AllowReleaseInfoChange::Version=true"
    # apt update could have problems such as key expirations, proceed anyway
    apt-get "${apt_opts}" update || true
  fi
}

# -------------------------------------

# it stores it locally
retrieve_pc_data() {

  # inspired by this -> src https://stackoverflow.com/questions/10358547/how-to-grep-for-contents-after-pattern/10358566#10358566
  manufacturer="$(sudo dmidecode -t1 | grep 'Manufacturer' | cut -d' ' -f2- | sed 's/ /_/g')"
  product="$(sudo dmidecode -t1 | grep 'Product Name' | cut -d' ' -f3- | sed 's/ /_/g')"
  sn="$(sudo dmidecode -t1 | grep 'Serial Number' | cut -d' ' -f3- | sed 's/ /_/g')"
  hwid="${manufacturer}__${product}__${sn}"
  ts="$(date +'%Y-%m-%d_%H-%M-%S')"
  myid="${hwid}__${ts}"

  pc_data_dir="${mydir}/pc_data/${myid}"
  mkdir -p "${pc_data_dir}"
  # TODO smartctl all disks:
  #   lsblk -p -o name | grep '^/' | grep -v '/dev/zd'
  if [ -b /dev/sda ] ; then
    smartctl -a /dev/sda > "${pc_data_dir}/smartctl.txt" || true
  elif [ -b /dev/vda ] ; then
    echo 'virtual disk from VM detected. No smart values' > "${pc_data_dir}/smartctl.txt"
  else
    echo "no /dev/sda nor /dev/vda detected. What is this?" > "${pc_data_dir}/smartctl.txt"
  fi

  dmidecode > "${pc_data_dir}/dmidecode.txt" || true
  ip link > "${pc_data_dir}/ip_link.txt" || true
  zip -rj "${pc_data_dir}/${myid}" \
      "${pc_data_dir}/smartctl.txt" \
      "${pc_data_dir}/dmidecode.txt" \
      "${pc_data_dir}/ip_link.txt" \
      || true

  summary_path="${mydir}/pc_data/summary.csv"
  # summary csv header
  echo 'date,lifetime,bios_vendor,bios_version,bios_release_date' > "${summary_path}"
  for file in $(find "${mydir}/pc_data" -mindepth 1 -maxdepth 1 -type d); do
    date="$(basename "${file}" | grep -o '[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}_[0-9]\{2\}-[0-9]\{2\}-[0-9]\{2\}$')" || true
    if [ "${date}" ]; then
       lifetime="$(cat "${file}/smartctl.txt" | grep Power_On_Hours | awk '{ print $10; }')"
       bios="$(cat "${file}/dmidecode.txt" \
                 | grep "BIOS Information" -A3 \
                 | grep -v "BIOS Information" \
                 | cut -d':' -f2 | paste -s -d',' | xargs | sed 's/, /,/g')"
       echo "${date},${lifetime},${bios}" >> "${summary_path}"
    fi
  done
  true
}

# disable ssh
post_cdist() {
  service sshd stop || service ssh stop || true
  # TODO do it better when systemd is not available
  systemctl disable ssh
}

cdist_config() {
  su -l "${myuser}" -c "${mydir}/run.sh --ext_debs localhost"
}

# prepare ssh for cdist
pre() {
  if [ "$(id -u)" -ne 0 ]; then
    echo "ERROR: run this script as root or sudo"
    exit 1
  fi

  # src https://stackoverflow.com/questions/3557037/appending-a-line-to-a-file-only-if-it-does-not-already-exist/3557165#3557165
  # modify if necessary to do efficient updates (only when needed)
  apt_file="/etc/apt/sources.list"
  # remove cdrom entries if exist
  apt_regex='^deb[[:space:]]+cdrom:.*$'
  grep -Eqx "${apt_regex}" "${apt_file}" && sed -i'' -E "s/${apt_regex}//g" /etc/apt/sources.list
  # add main repository if it does not exist
  apt_regex='^deb.*http://.*/debian.*bullseye main.*$'
  apt_line="deb http://deb.debian.org/debian bullseye main contrib non-free"
  grep -Eqx "${apt_regex}" "${apt_file}" || sed -i'' -E "1i ${apt_line}" "${apt_file}"
  # comment existing deb-src lines to optimize update
  apt_regex='^deb-src'
  grep -Eqx "${apt_regex}" "${apt_file}" && sed -i'' -E "s/${apt_regex}/# deb-src/g" "${apt_file}"
  # TODO 2022-1-28 temp backward compatible measure before all stations upgrade
  rm -f /etc/apt/sources.list.d/microsoft-edge-beta.list

  decide_if_update

  # workaround when packages are broken
  apt --fix-broken -y install || true

  # Basic system utilities
  apt-get install -y ssh wget curl git netcat-openbsd smartmontools util-linux dmidecode zip iproute2
}

pre_cdist() {
  # This is an environment variable that defaults to $HOME/.ssh/id_rsa.pub
  export SSH_KEY="${SSH_KEY:-$(get_user_home "${myuser}")/.ssh/id_rsa.pub}"

  prepare_ssh_dir "${myuser}"

  sshconfig="/home/${myuser}/.ssh/config"
  if ! grep -qs "# configurador: do not check localhost" "${sshconfig}"; then
      cat >> "${sshconfig}" <<EOF
# configurador: do not check localhost
Host localhost
    StrictHostKeyChecking no
    UserKnownHostsFile=/dev/null
EOF
  fi

  # Harden users a bit
  #   PasswordAuthentication no
  sed -i'' -E 's/^[#]?[[:space:]]*PasswordAuthentication.*/PasswordAuthentication no/g' /etc/ssh/sshd_config

  # Allow root access for provisioning
  #   PermitRootLogin prohibit-password
  sed -i'' -E 's/^[#]?[[:space:]]*PermitRootLogin.*/PermitRootLogin prohibit-password/g' /etc/ssh/sshd_config

  # add my ssh key
  prepare_ssh_dir "root"

  # It could be that ssh has not generated the keys so
  #   a restart would not work as expected
  service sshd stop || service ssh stop || true
  service sshd start || service ssh start

  # Lower GRUB timeout
  if grep -qs "GRUB_TIMEOUT=5" /etc/default/grub; then
    sed -i'' -E 's/^[#]?[[:space:]]*GRUB_TIMEOUT=[0-9]+$/GRUB_TIMEOUT=1/g' /etc/default/grub
    # reload grub config
    update-grub
  fi
}

info() {
  text="$(cat <<EOF

  Este programa reconfigura y actualiza el sistema operativo para que tu equipo
  esté al día.

  Verás cómo se ejecutan una serie de comandos en la consola. Ten paciencia.

  Cuando veas una línea con la palabra "Fin" cierra la consola y ya podrás
  continuar trabajando.

  Cualquier duda o problema, dirígete al canal de Telegram.




    Pulsa enter para continuar o cierra la ventana para cancelar




EOF
)"

  echo "${text}"
}

check_os() {
  . /etc/os-release

  if [ ! "${VERSION_CODENAME}" = "bullseye" ]; then
    true
  fi
}

# ----------------------

main() {
  info
  # press any key to continue
  read enter_key

  if [ "${DEBUG}" = 'y' ]; then
    set -x
  fi

  # install all prerequisites, etc.
  pre

  # TODO looks like a generic function
  # TODO hardcoded: this script assumes that the principal user is 1000
  myuser="$(cat /etc/passwd | grep 1000 | cut -d':' -f1)"
  mydir="/home/${myuser}/configurador__basic-xfce-desktop"
  fetch_git_repo https://gitlab.com/dsg-upc/basic-xfce-desktop.git "${mydir}"
  chown -R ${myuser}: "${mydir}"

  # default values
  CONFIGURE="${CONFIGURE:-y}"
  LIFETIME="${LIFETIME:-y}"

  if [ "${CONFIGURE}" = 'y' ]; then
    pre_cdist
    cdist_config
    post_cdist
  fi

  if [ "${LIFETIME}" = 'y' ]; then
    retrieve_pc_data
  fi

  echo "\n\n\n\n  Fin\n\n\n\n"
}

main "${@}"
