#!/bin/sh -e

# src https://coderwall.com/p/hes3ha/change-the-dependencies-of-a-deb-package
discord_in="discord.deb"
discord_out="discord.deb"
if dpkg -I "${discord_in}" | grep -q libayatana-appindicator1 ; then
  echo '[discord] INFO: bypass postinstall process'
  return 0
fi
ar x "${discord_in}"
tar xzf control.tar.gz
sed -i 's/libappindicator1/libayatana-appindicator1/' control
tar c postinst control | gzip -c > control.tar.gz
ar rcs "${discord_out}" debian-binary control.tar.gz data.tar.gz
rm -f debian-binary data.tar.gz control.tar.gz control
#sudo dpkg -I "${discord_out}"
