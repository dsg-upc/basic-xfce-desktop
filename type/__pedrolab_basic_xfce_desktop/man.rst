cdist-type__pedrolab_basic_xfce_desktop(7)
===============================

NAME
----
cdist-type__pedrolab_basic_xfce_desktop - unbound server deployment for ungleich

in one case a laptop with intel graphics needed this configuration

Problem on some intel graphic cards
-----------

In some situations, certain laptops have problems with intel graphic cards that can be solved this way

.. code-block:: sh

    /etc/X11/xorg.conf.d/20-intel.conf

    Section "Device"
       Identifier  "Intel Graphics"
       Driver      "intel"
       Option      "TearFree"    "true"
    EndSection

thanks https://wiki.archlinux.org/index.php/Intel_graphics_(Espa%C3%B1ol)#V%C3%ADdeo_rasgado
